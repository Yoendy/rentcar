Vue.config.devtools = true;
Vue.config.debug = true;

var detalle =
    {
        id: 0,
        facturaId: 0,
        cantidad: 1,
        servicioId: 1,
        monto: 0,
        itbis: 0,
        subTotal: 0,
        total: 0
};
var model = {
    id: 0,
    version: 0,
    clienteId: 1,
    destino: 'Punta Cana',
    cotizacion: false,
    tipoFactura: 1,
    conComprobanteFiscal: false,
    comprobanteFiscal: '',
    descuento: 0,
    estatus: "P",
    estado: 1,
    detail: detalle,
    detalleFactura:[]
};

var app = new Vue({
    el: '#formFactura',
    data:  {factura: model},
    computed:
        {


        },
    watch:
        {
            'factura.detail.cantidad': function ()
            {
                if(this.factura.detail != undefined)
                {
                    this.factura.detail.subTotal = parseFloat(parseFloat(this.factura.detail.cantidad) * parseFloat(this.factura.detail.monto));
                    this.factura.detail.itbis = parseFloat(parseFloat(this.factura.detail.subTotal) * 0.18);
                    this.factura.detail.total = parseFloat(parseFloat(this.factura.detail.subTotal)+ parseFloat(this.factura.detail.itbis));
                }

            },
            'factura.detail.monto': function ()
            {
                if(this.factura.detail != undefined)
                {
                    this.factura.detail.subTotal = parseFloat(parseFloat(this.factura.detail.cantidad) * parseFloat(this.factura.detail.monto));
                    this.factura.detail.itbis = parseFloat(parseFloat(parseFloat(this.factura.detail.cantidad) * parseFloat(this.factura.detail.monto)) * 0.18);
                    this.factura.detail.total = parseFloat(parseFloat(this.factura.detail.subTotal) + parseFloat(this.factura.detail.itbis));
                }
            },
            'factura.detalleFactura': function ()
            {
                if(this.factura.detalleFactura != undefined)
                {
                    this.factura.total =  sum(this.factura.detalleFactura,  function (x)
                    {
                        return (x.total)
                    });

                    this.factura.subTotal = sum(this.factura.detalleFactura,  function (x)
                    {
                        return (x.subTotal)
                    });

                    this.factura.itbis = sum(this.factura.detalleFactura,  function (x)
                    {
                        return (x.itbis)
                    });

                }
            }
    },
    methods:
        {
            guardar: function ()
            {
                console.log(model);
                axios.post("/api/facturas/",
                    {
                        id: model.id,
                        version: model.version,
                        clienteId: model.clienteId,
                        destino: model.destino,
                        cotizacion: model.cotizacion,
                        tipoFactura: model.tipoFactura,
                        conComprobanteFiscal: model.conComprobanteFiscal,
                        comprobanteFiscal: model.comprobanteFiscal,
                        descuento: model.descuento,
                        estatus: model.estatus,
                        estado: model.estado,
                        detalleFactura: model.detalleFactura,
                        itbis: model.itbis,
                        subTotal: model.subTotal,
                        total: model.total
                    })
                    .then(response => {
                        // JSON responses are automatically parsed.
                        console.log(response.data)
                        this.factura.id = response.data.id;
                        //noinspection JSAnnotator

                    })
                    .catch(e => {
                        console.log(e);
                    });


            },
            getOne: function (id)
            {
                var self = this;
                if(id != undefined && id != "crear")
                {
                    axios.get("/api/facturas/"+id)
                        .then(response => {
                            // JSON responses are automatically parsed.
                            console.log(response.data)
                            //noinspection JSAnnotator
                            self.factura = response.data;
                            self.factura.detail = detalle;
                        })
                        .catch(e => {
                            console.log(e);
                        });
                }
            },
            getAll: function ()
            {
                this.$http.get("/facturas/getOne",{id: id}, function(response)
                {
                    this.$set('model',response);
                });


            },
            agregar: function ()
            {
                var detalle =
                    {
                        id: 0,
                        facturaId: this.factura.id,
                        cantidad: this.factura.detail.cantidad,
                        servicioId: this.factura.detail.servicioId,
                        monto: this.factura.detail.monto,
                        itbis: this.factura.detail.itbis,
                        subTotal: this.factura.detail.subTotal,
                        total: this.factura.detail.total
                     };

                this.factura.detalleFactura.push(detalle);
            },
            remover: function (item)
            {
                this.factura.detalleFactura.splice(this.factura.detalleFactura.indexOf(item),1);

                if(item.id != undefined) return;

                axios.delete("/api/facturas/detalleFactura"+item.id)
                .then(response =>
                {
                    console.log(response.data);
                    this.modificar(this.factura);
                })
                .catch(e =>
                {
                    console.log(e);
                });
            },
            editar: function (item)
            {
                this.factura.detalleFactura.splice(this.factura.detalleFactura.indexOf(item),1);
                this.factura.detail.id = item.id;
                this.factura.detail.facturaId = item.facturaId;
                this.factura.detail.cantidad = item.cantidad;
                this.factura.detail.servicioId = item.servicioId;
                this.factura.detail.monto= item.monto;
                this.factura.detail.itbis = item.itbis;
                this.factura.detail.subTotal = item.subTotal;
                this.factura.detail.total = item.total;
            },
            cargar: function ()
            {
                var pathArray = window.location.pathname.split( '/' ).reverse();
                var id = pathArray[0];
                this.getOne(id);
            },
            modificar: function (factura)
            {
                console.log(this.factura);
                axios.put("/api/facturas/"+this.factura.id,
                    {
                        id: this.factura.id,
                        version: this.factura.version,
                        clienteId: this.factura.clienteId,
                        destino: this.factura.destino,
                        cotizacion: this.factura.cotizacion,
                        tipoFactura: this.factura.tipoFactura,
                        conComprobanteFiscal: this.factura.conComprobanteFiscal,
                        comprobanteFiscal: this.factura.comprobanteFiscal,
                        descuento: this.factura.descuento,
                        estatus: this.factura.estatus,
                        estado: this.factura.estado,
                        detalleFactura: this.factura.detalleFactura,
                        itbis: this.factura.itbis,
                        subTotal: this.factura.subTotal,
                        total: this.factura.total
                    })
                    .then(response => {
                        // JSON responses are automatically parsed.
                        console.log(response.data)
                        //noinspection JSAnnotator

                    })
                    .catch(e => {
                        console.log(e);
                    });


            },
        },
    mounted: function ()
        {
            var pathArray = window.location.pathname.split( '/' ).reverse();
            var id = pathArray[0];
            this.getOne(id);
        }

});

function sum(source, expresion)
{
    var total = 0;

    if(source == undefined) return total;

    for (var i = 0; i < source.length; i++)
    {
        total += parseFloat(expresion(source[i]));
    }
    return total;
}

function actualizarCliente(id)
{
    console.log(id);
    model.clienteId = id;
}

