/**
 * Created by ymaldonado on 7/14/2017.
 */
Vue.component('item',
    {
        props: ['value'],
        template:
            `<td >{{value}}</td>
            `
    });

Vue.component('clientes',
    {
        props: ['clientes','total'],
        template: `<table>
        <thead>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
            </tr>
        </thead>
        <tbody>
            <tr v-for="item in clientes">
                <item v-bind:value="item.codigo"></item>
                <item v-bind:value="item.nombre"></item>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td colspan={2}>
                    Total : <span>{{total}}</span>
                </td>
            </tr>
        </tfoot>
</table>`
    });

var cliente = {total: 0, clientes: []}

var appCliente = new Vue({
    el: '#cliente',
    data: cliente,
    methods: {
        getAll: function () {

            axios.get("http://10.228.30.87:7001/api/Facturacion/Clientes?fields=codigo,nombre")
                .then(response => {
                // JSON responses are automatically parsed.
                console.log(response.data.data)
                this.clientes = response.data.data;
            })
                .catch(e => {
                    console.log(e);
                })
        }
    },
    components: {

    }
});