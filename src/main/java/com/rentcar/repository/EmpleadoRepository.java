package com.rentcar.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.rentcar.domain.Empleado;
@RepositoryRestResource(path = "Empleados")
public interface EmpleadoRepository extends PagingAndSortingRepository<Empleado, Long>{

}
