package com.rentcar.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import com.rentcar.domain.*;

@RepositoryRestResource(path = "Clientes")
public interface ClienteRepository extends CrudRepository<Cliente, Long>
{

}
