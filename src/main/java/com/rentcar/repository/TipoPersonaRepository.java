package com.rentcar.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.rentcar.domain.TipoPersona;

@RepositoryRestResource(path = "TipoPersonas")
public interface TipoPersonaRepository extends PagingAndSortingRepository<TipoPersona, Long>{

}
