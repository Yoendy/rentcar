package com.rentcar.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.rentcar.domain.Inspeccion;

@RepositoryRestResource(path = "Inspecciones")
public interface InspeccionRepository extends PagingAndSortingRepository<Inspeccion, Long> 
{

}
