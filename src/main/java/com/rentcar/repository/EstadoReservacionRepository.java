package com.rentcar.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.rentcar.domain.EstadoReservacion;

@RepositoryRestResource(path = "EstadoReservaciones")
public interface EstadoReservacionRepository extends PagingAndSortingRepository<EstadoReservacion, Long> {

}
