package com.rentcar.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.rentcar.domain.TandaLaboral;

@RepositoryRestResource(path = "TandasLaborales")
public interface TandaLaboralRepository extends PagingAndSortingRepository<TandaLaboral, Long>{

}
