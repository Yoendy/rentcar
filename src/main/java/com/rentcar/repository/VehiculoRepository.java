package com.rentcar.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.rentcar.domain.Vehiculo;

@RepositoryRestResource(path = "Vehiculos")
public interface VehiculoRepository extends PagingAndSortingRepository<Vehiculo, Long>{



}
