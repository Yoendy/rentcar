package com.rentcar.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.rentcar.domain.TipoVehiculo;

@RepositoryRestResource(path = "TipoVehiculos")
public interface TipoVehiculoRepository extends PagingAndSortingRepository<TipoVehiculo, Long>{


}
