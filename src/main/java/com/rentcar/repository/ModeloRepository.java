package com.rentcar.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.rentcar.domain.Modelo;

@RepositoryRestResource(path = "Modelos")
public interface ModeloRepository  extends PagingAndSortingRepository<Modelo, Long>{

}
