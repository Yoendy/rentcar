package com.rentcar.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.rentcar.domain.Reservacion;

@RepositoryRestResource(path = "Reservaciones")
public interface ReservacionRepository  extends PagingAndSortingRepository<Reservacion, Long>{

}
