package com.rentcar.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.rentcar.domain.Marca;

@RepositoryRestResource(path = "Marcas")
public interface MarcaRepository  extends PagingAndSortingRepository<Marca, Long>{

}
