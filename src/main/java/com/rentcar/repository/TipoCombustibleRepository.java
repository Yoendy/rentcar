package com.rentcar.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.rentcar.domain.TipoCombustible;

@RepositoryRestResource(path = "TipoCombustibles")
public interface TipoCombustibleRepository extends PagingAndSortingRepository<TipoCombustible, Long>{

}
