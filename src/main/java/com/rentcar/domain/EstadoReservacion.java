package com.rentcar.domain;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="EstadoReservaciones")
public class EstadoReservacion extends BaseDomain
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long Id;

    private String Nombre;

    private String Descripcion;

    private Boolean Activo;

    @OneToMany(mappedBy = "EstadoReservacion", cascade = CascadeType.ALL)
    private Set<Reservacion> Reservaciones = new HashSet<>();

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }

    public Boolean getActivo() {
        return Activo;
    }

    public void setActivo(Boolean activo) {
        Activo = activo;
    }

    public Set<Reservacion> getReservaciones() {
        return Reservaciones;
    }

    public void setReservaciones(Set<Reservacion> reservaciones) {
        Reservaciones = reservaciones;
    }
}
