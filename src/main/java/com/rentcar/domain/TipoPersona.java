package com.rentcar.domain;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="TipoPersonas")
public class TipoPersona extends BaseDomain
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long Id;

    private String Nombre;

    private Boolean Activo;

    @OneToMany(mappedBy = "TipoPersona", cascade = CascadeType.ALL)
    private Set<Cliente> Clientes = new HashSet<Cliente>();

    public Long getId() {
        return Id;
    }


    public void setId(Long id) {
        Id = id;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }


    public Boolean getActivo() {
        return Activo;
    }

    public void setActivo(Boolean activo) {
        Activo = activo;
    }

    public Set<Cliente> getClientes() {
        return Clientes;
    }

    public void setClientes(Set<Cliente> clientes) {
        Clientes = clientes;
    }
}
