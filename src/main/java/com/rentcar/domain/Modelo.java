package com.rentcar.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;


@Entity
@Table(name="Modelos")
public class Modelo extends BaseDomain
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long Id;
	
	private String Nombre;
	
	private String Descripcion;
	
	@ManyToOne
	@JoinColumn(name="MarcaId")
	private Marca Marca;
	
	@OneToMany(mappedBy = "Modelo", cascade = CascadeType.ALL)
	private Set<Vehiculo> Vehiculos = new HashSet<Vehiculo>();
	
	private Boolean Activo;

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public String getNombre() {
		return Nombre;
	}

	public void setNombre(String nombre) {
		Nombre = nombre;
	}

	public String getDescripcion() {
		return Descripcion;
	}

	public void setDescripcion(String descripcion) {
		Descripcion = descripcion;
	}

	public com.rentcar.domain.Marca getMarca() {
		return Marca;
	}

	public void setMarca(com.rentcar.domain.Marca marca) {
		Marca = marca;
	}

	public Set<Vehiculo> getVehiculos() {
		return Vehiculos;
	}

	public void setVehiculos(Set<Vehiculo> vehiculos) {
		Vehiculos = vehiculos;
	}

	public Boolean getActivo() {
		return Activo;
	}

	public void setActivo(Boolean activo) {
		Activo = activo;
	}
}
