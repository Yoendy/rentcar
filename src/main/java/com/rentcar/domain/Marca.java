package com.rentcar.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;


@Entity
@Table(name="Marcas")
public class Marca extends BaseDomain
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long Id;
	
	private String Nombre;
	
	private String Descripcion;
	
	private String Imagen;
	
	private Boolean Activo;	
	
	@OneToMany(mappedBy = "Marca", cascade = CascadeType.ALL)
	private Set<Modelo> Modelos = new HashSet<Modelo>();

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public String getNombre() {
		return Nombre;
	}

	public void setNombre(String nombre) {
		Nombre = nombre;
	}

	public String getDescripcion() {
		return Descripcion;
	}

	public void setDescripcion(String descripcion) {
		Descripcion = descripcion;
	}

	public String getImagen() {
		return Imagen;
	}

	public void setImagen(String imagen) {
		Imagen = imagen;
	}

	public Boolean getActivo() {
		return Activo;
	}

	public void setActivo(Boolean activo) {
		Activo = activo;
	}

	public Set<Modelo> getModelos() {
		return Modelos;
	}

	public void setModelos(Set<Modelo> modelos) {
		Modelos = modelos;
	}
}
