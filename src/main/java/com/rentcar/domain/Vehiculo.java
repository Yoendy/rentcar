package com.rentcar.domain;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name="Vehiculos")
public class Vehiculo extends BaseDomain{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long Id;
	
	private String Descripcion;
	
	private String NoChasis;

	private String NoMotor;

	private String NoPlaca;
	
	@ManyToOne
	@JoinColumn(name="ModeloId")
	private Modelo Modelo;
	
	@ManyToOne
	@JoinColumn(name="TipoCombustibleId")
	private TipoCombustible TipoCombustible;
	
	private int Anio;
	
	private String Color;
	
	private BigDecimal Tarifa;
	
	private String Imagen;
	
	private int Estado;
	
	private Boolean Activo = true;

	@OneToMany(mappedBy = "Vehiculo", cascade = CascadeType.ALL)
	private Set<Inspeccion> Inspecciones = new HashSet<>();
	
	@OneToMany(mappedBy = "Vehiculo", cascade = CascadeType.ALL)
	private Set<Reservacion> Reservaciones = new HashSet<>();

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public String getDescripcion() {
		return Descripcion;
	}

	public void setDescripcion(String descripcion) {
		Descripcion = descripcion;
	}

	public String getNoChasis() {
		return NoChasis;
	}

	public void setNoChasis(String noChasis) {
		NoChasis = noChasis;
	}

	public String getNoMotor() {
		return NoMotor;
	}

	public void setNoMotor(String noMotor) {
		NoMotor = noMotor;
	}

	public String getNoPlaca() {
		return NoPlaca;
	}

	public void setNoPlaca(String noPlaca) {
		NoPlaca = noPlaca;
	}

	public com.rentcar.domain.Modelo getModelo() {
		return Modelo;
	}

	public void setModelo(com.rentcar.domain.Modelo modelo) {
		Modelo = modelo;
	}

	public com.rentcar.domain.TipoCombustible getTipoCombustible() {
		return TipoCombustible;
	}

	public void setTipoCombustible(com.rentcar.domain.TipoCombustible tipoCombustible) {
		TipoCombustible = tipoCombustible;
	}

	public int getAnio() {
		return Anio;
	}

	public void setAnio(int anio) {
		Anio = anio;
	}

	public String getColor() {
		return Color;
	}

	public void setColor(String color) {
		Color = color;
	}

	public BigDecimal getTarifa() {
		return Tarifa;
	}

	public void setTarifa(BigDecimal tarifa) {
		Tarifa = tarifa;
	}

	public String getImagen() {
		return Imagen;
	}

	public void setImagen(String imagen) {
		Imagen = imagen;
	}

	public int getEstado() {
		return Estado;
	}

	public void setEstado(int estado) {
		Estado = estado;
	}

	public Boolean getActivo() {
		return Activo;
	}

	public void setActivo(Boolean activo) {
		Activo = activo;
	}

	public Set<Inspeccion> getInspecciones() {
		return Inspecciones;
	}

	public void setInspecciones(Set<Inspeccion> inspecciones) {
		Inspecciones = inspecciones;
	}

	public Set<Reservacion> getReservaciones() {
		return Reservaciones;
	}

	public void setReservaciones(Set<Reservacion> reservaciones) {
		Reservaciones = reservaciones;
	}
	
	
}
