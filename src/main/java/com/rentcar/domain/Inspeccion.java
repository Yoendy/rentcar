package com.rentcar.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="Inspecciones")
public class Inspeccion extends BaseDomain{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long Id;

    private String Descripcion;

    @ManyToOne
    @JoinColumn(name="VehiculoId")
    private Vehiculo Vehiculo;

    @ManyToOne
    @JoinColumn(name="ClienteId")
    private Cliente Cliente;

    @ManyToOne
    @JoinColumn(name="EmpleadoId")
    private Empleado Empleado;

    private Boolean TieneRalladuras;

    private String CantidadCombustible;
    private Boolean TieneGomaRepuesto;
    private Boolean TieneGato;
    private Boolean TieneRoturaCristal;
    private Boolean EstadoGomas;
    private Date Fecha;

    private Boolean Activo;


    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }

    public com.rentcar.domain.Vehiculo getVehiculo() {
        return Vehiculo;
    }

    public void setVehiculo(com.rentcar.domain.Vehiculo vehiculo) {
        Vehiculo = vehiculo;
    }

    public com.rentcar.domain.Cliente getCliente() {
        return Cliente;
    }

    public void setCliente(com.rentcar.domain.Cliente cliente) {
        Cliente = cliente;
    }

    public com.rentcar.domain.Empleado getEmpleado() {
        return Empleado;
    }

    public void setEmpleado(com.rentcar.domain.Empleado empleado) {
        Empleado = empleado;
    }

    public Boolean getTieneRalladuras() {
        return TieneRalladuras;
    }

    public void setTieneRalladuras(Boolean tieneRalladuras) {
        TieneRalladuras = tieneRalladuras;
    }

    public String getCantidadCombustible() {
        return CantidadCombustible;
    }

    public void setCantidadCombustible(String cantidadCombustible) {
        CantidadCombustible = cantidadCombustible;
    }

    public Boolean getTieneGomaRepuesto() {
        return TieneGomaRepuesto;
    }

    public void setTieneGomaRepuesto(Boolean tieneGomaRepuesto) {
        TieneGomaRepuesto = tieneGomaRepuesto;
    }

    public Boolean getTieneGato() {
        return TieneGato;
    }

    public void setTieneGato(Boolean tieneGato) {
        TieneGato = tieneGato;
    }

    public Boolean getTieneRoturaCristal() {
        return TieneRoturaCristal;
    }

    public void setTieneRoturaCristal(Boolean tieneRoturaCristal) {
        TieneRoturaCristal = tieneRoturaCristal;
    }

    public Boolean getEstadoGomas() {
        return EstadoGomas;
    }

    public void setEstadoGomas(Boolean estadoGomas) {
        EstadoGomas = estadoGomas;
    }

    public Date getFecha() {
        return Fecha;
    }

    public void setFecha(Date fecha) {
        Fecha = fecha;
    }

    public Boolean getActivo() {
        return Activo;
    }

    public void setActivo(Boolean activo) {
        Activo = activo;
    }
}
