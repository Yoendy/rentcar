package com.rentcar.domain;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="Empleados")
public class Empleado extends BaseDomain{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long Id;

    private String Nombre;

    private String Cedula;

    @ManyToOne
    @JoinColumn(name="TandaLaboralId")
    private TandaLaboral TandaLaboral;

    private Double PorcientoComision;

    private Date FechaIngreso;

    private Boolean Activo;

    @OneToMany(mappedBy = "Empleado", cascade = CascadeType.ALL)
    private Set<Inspeccion> Inspecciones = new HashSet<>();
    
    @OneToMany(mappedBy = "Empleado", cascade = CascadeType.ALL)
    private Set<Reservacion> Reservaciones = new HashSet<>();

    

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getCedula() {
        return Cedula;
    }

    public void setCedula(String cedula) {
        Cedula = cedula;
    }

    public com.rentcar.domain.TandaLaboral getTandaLaboral() {
        return TandaLaboral;
    }

    public void setTandaLaboral(com.rentcar.domain.TandaLaboral tandaLaboral) {
        TandaLaboral = tandaLaboral;
    }

    public Double getPorcientoComision() {
        return PorcientoComision;
    }

    public void setPorcientoComision(Double porcientoComision) {
        PorcientoComision = porcientoComision;
    }

    public Date getFechaIngreso() {
        return FechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        FechaIngreso = fechaIngreso;
    }


    public Boolean getActivo() {
        return Activo;
    }

    public void setActivo(Boolean activo) {
        Activo = activo;
    }

    public Set<Inspeccion> getInspecciones() {
        return Inspecciones;
    }

    public void setInspecciones(Set<Inspeccion> inspecciones) {
        Inspecciones = inspecciones;
    }

	public Set<Reservacion> getReservaciones() {
		return Reservaciones;
	}

	public void setReservaciones(Set<Reservacion> reservaciones) {
		Reservaciones = reservaciones;
	}
}
