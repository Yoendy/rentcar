package com.rentcar.domain;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name="Reservaciones")
public class Reservacion extends BaseDomain{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long Id;

    @ManyToOne
    @JoinColumn(name="VehiculoId")
    private Vehiculo Vehiculo;

    @ManyToOne
    @JoinColumn(name="ClienteId")
    private Cliente Cliente;

    @ManyToOne
    @JoinColumn(name="EmpleadoId")
    private Empleado Empleado;

    private Date FechaRenta;

    private Date FechaDevolucion;

    private BigDecimal MontoPorDia;

    private int CantidadDias;

    private String Comentarios;

    @ManyToOne
    @JoinColumn(name="EstadoReservacionId")
    private EstadoReservacion EstadoReservacion;

    private Boolean Activo;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public com.rentcar.domain.Vehiculo getVehiculo() {
        return Vehiculo;
    }

    public void setVehiculo(com.rentcar.domain.Vehiculo vehiculo) {
        Vehiculo = vehiculo;
    }

    public com.rentcar.domain.Cliente getCliente() {
        return Cliente;
    }

    public void setCliente(com.rentcar.domain.Cliente cliente) {
        Cliente = cliente;
    }

    public com.rentcar.domain.Empleado getEmpleado() {
        return Empleado;
    }

    public void setEmpleado(com.rentcar.domain.Empleado empleado) {
        Empleado = empleado;
    }

    public Date getFechaRenta() {
        return FechaRenta;
    }

    public void setFechaRenta(Date fechaRenta) {
        FechaRenta = fechaRenta;
    }

    public Date getFechaDevolucion() {
        return FechaDevolucion;
    }

    public void setFechaDevolucion(Date fechaDevolucion) {
        FechaDevolucion = fechaDevolucion;
    }

    public BigDecimal getMontoPorDia() {
        return MontoPorDia;
    }

    public void setMontoPorDia(BigDecimal montoPorDia) {
        MontoPorDia = montoPorDia;
    }

    public int getCantidadDias() {
        return CantidadDias;
    }

    public void setCantidadDias(int cantidadDias) {
        CantidadDias = cantidadDias;
    }

    public String getComentarios() {
        return Comentarios;
    }

    public void setComentarios(String comentarios) {
        Comentarios = comentarios;
    }

    public com.rentcar.domain.EstadoReservacion getEstadoReservacion() {
        return EstadoReservacion;
    }

    public void setEstadoReservacion(com.rentcar.domain.EstadoReservacion estadoReservacion) {
        EstadoReservacion = estadoReservacion;
    }

    public Boolean getActivo() {
        return Activo;
    }

    public void setActivo(Boolean activo) {
        Activo = activo;
    }
}
