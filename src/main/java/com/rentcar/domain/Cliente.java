package com.rentcar.domain;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;


@Entity
@Table(name="Clientes")
public class Cliente extends BaseDomain{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long Id;

    private String Nombre;

    private String Cedula;

    private String NoTarjetaCredito;

    private BigDecimal LimiteCredito;

    private Boolean Activo;

    @ManyToOne
    @JoinColumn(name="TipoPersonaId")
    private TipoPersona TipoPersona;

    @OneToMany(mappedBy = "Cliente", cascade = CascadeType.ALL)
    private Set<Inspeccion> Inspecciones = new HashSet<>();
    
    @OneToMany(mappedBy = "Cliente", cascade = CascadeType.ALL)
    private Set<Reservacion> Reservaciones = new HashSet<>();


    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getCedula() {
        return Cedula;
    }

    public void setCedula(String cedula) {
        Cedula = cedula;
    }

    public String getNoTarjetaCredito() {
        return NoTarjetaCredito;
    }

    public void setNoTarjetaCredito(String noTarjetaCredito) {
        NoTarjetaCredito = noTarjetaCredito;
    }

    public BigDecimal getLimiteCredito() {
        return LimiteCredito;
    }

    public void setLimiteCredito(BigDecimal limiteCredito) {
        LimiteCredito = limiteCredito;
    }

    public Boolean getActivo() {
        return Activo;
    }


    public void setActivo(Boolean activo) {
        Activo = activo;
    }

    public com.rentcar.domain.TipoPersona getTipoPersona() {
        return TipoPersona;
    }

    public void setTipoPersona(com.rentcar.domain.TipoPersona tipoPersona) {
        TipoPersona = tipoPersona;
    }

    public Set<Inspeccion> getInspecciones() {
        return Inspecciones;
    }

    public void setInspecciones(Set<Inspeccion> inspecciones) {
        Inspecciones = inspecciones;
    }

	public Set<Reservacion> getReservaciones() {
		return Reservaciones;
	}

	public void setReservaciones(Set<Reservacion> reservaciones) {
		Reservaciones = reservaciones;
	}
    
    
}
