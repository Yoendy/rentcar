package com.rentcar.domain;

import java.util.Date;

import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.MappedSuperclass;
import javax.persistence.Id;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@MappedSuperclass
@EntityListeners({AuditingEntityListener.class})
public class BaseDomain 
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long Id;
	
	private Boolean Activo;
	
	@CreatedDate
    private Date CreadoEn;
	
    @CreatedBy
    private String CreadoPor;
    
    @LastModifiedDate
    private Date ModificadoEn;
    
    @LastModifiedBy
    private String ModificadoPor;

}
