package com.rentcar.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;


@Entity
@Table(name="TipoCombustibles")
public class TipoCombustible
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long Id;
	
	private String Nombre;
	
	private String Descripcion;
	
	private Boolean Activo;
	
	@OneToMany(mappedBy = "TipoCombustible", cascade = CascadeType.ALL)
	private Set<Vehiculo> Vehiculos = new HashSet<Vehiculo>();

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public String getNombre() {
		return Nombre;
	}

	public void setNombre(String nombre) {
		Nombre = nombre;
	}

	public String getDescripcion() {
		return Descripcion;
	}

	public void setDescripcion(String descripcion) {
		Descripcion = descripcion;
	}


	public Boolean getActivo() {
		return Activo;
	}

	public void setActivo(Boolean activo) {
		Activo = activo;
	}

	public Set<Vehiculo> getVehiculos() {
		return Vehiculos;
	}

	public void setVehiculos(Set<Vehiculo> vehiculos) {
		Vehiculos = vehiculos;
	}
}